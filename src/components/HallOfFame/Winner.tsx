import React, { FC } from 'react';

import { Name, Score } from './HallOfFame.atoms';
import { HallOfFameUser } from './HallOfFame.types';

const Winner: FC<{
  data: HallOfFameUser;
}> = ({ data }) => {
  return (
    <tr>
      <Name>{data.name}</Name>
      <Score>{data.score}</Score>
    </tr>
  );
};

export default Winner;
