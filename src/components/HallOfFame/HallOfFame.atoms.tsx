import { transparentize } from 'polished';
import React from 'react';
import styled from 'styled-components';

export const Container = styled.div`
  max-width: ${(props) => props.theme.areaSize}px;
  margin: auto;
`;

export const Header = styled.div`
  font-family: ${(props) => props.theme.specialFont};
  letter-spacing: 0.11rem;
  text-transform: uppercase;
  margin-bottom: 1rem;
`;

export const Input = styled.input`
  border: none;
  border-bottom: 1px solid ${(props) => props.theme.fontColor};
  color: ${(props) => props.theme.fontColor};
  background: none;
  margin-left: 0.5rem;
  outline: none;
  font-family: ${(props) => props.theme.regularFont};
`;

export const ButtonContainer = styled.div`
  margin-top: 3rem;
`;

export const Button = styled.button`
  text-transform: uppercase;
  font-family: ${(props) => props.theme.regularFont};
  font-weight: 700;
  letter-spacing: 2px;
  border: none;
  background: ${(props) => props.theme.fontColor};
  padding: 1rem 5rem;
  outline: none;
  position: relative;
  color: ${(props) => props.theme.bodyBackground};
  cursor: pointer;

  &:hover {
    color: ${(props) => transparentize(0.3, props.theme.bodyBackground)};
  }

  &:before,
  &:after {
    content: '';
    display: block;
    position: absolute;
    box-sizing: border-box;
  }

  &:before {
    border-bottom: 1px solid ${(props) => props.theme.fontColor};
    border-left: 1px solid ${(props) => props.theme.fontColor};
    bottom: -3px;
    height: 3px;
    left: 3px;
    width: 100%;
  }

  &:after {
    border-right: 1px solid ${(props) => props.theme.fontColor};
    border-top: 1px solid ${(props) => props.theme.fontColor};
    height: 100%;
    right: -3px;
    top: 3px;
    width: 3px;
  }
`;

export const Title = styled.div`
  text-align: center;
  font-weight: 700;
  text-transform: uppercase;
  font-size: 3rem;
  margin-bottom: 1rem;

  &:after {
    content: '';
    height: 45px;
    display: block;

    background: linear-gradient(to bottom, white, white 50%, black 50%, black);
    background-size: 100% 15px;
  }
`;

export const Table = styled.table`
  width: 100%;
`;

export const Name = styled.td`
  text-transform: uppercase;
  color: ${(props) => transparentize(0.5, props.theme.fontColor)};
`;

export const Score = styled.td`
  text-align: right;
`;

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export const Th = styled(({ align, ...props }) => {
  return <th {...props} />;
})`
  text-align: ${(props) => props.align};
`;
