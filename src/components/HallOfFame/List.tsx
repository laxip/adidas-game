import React, { FC, useCallback, useContext, useEffect, useState } from 'react';
import { useResetRecoilState } from 'recoil';
import ServicesContext, { HALL_OF_FAME_SERVICE } from 'services';
import { gameLevelState } from 'states/gameLevelState';
import { showHallOfFameState } from 'states/showHallOfFameState';

import { Table, Title, Th, Button, ButtonContainer } from './HallOfFame.atoms';
import { HallOfFameService, HallOfFameUser } from './HallOfFame.types';
import Winner from './Winner';

const List: FC = () => {
  const service: HallOfFameService = useContext(ServicesContext)[HALL_OF_FAME_SERVICE];

  const resetGameLevelState = useResetRecoilState(gameLevelState);
  const resetHallOfFameState = useResetRecoilState(showHallOfFameState);

  const [list, setList] = useState<HallOfFameUser[]>([]);

  useEffect(() => {
    async function fetchData() {
      setList(await service.getList());
    }

    fetchData();
  }, [service]);

  const handlePlayAgain = useCallback(() => {
    resetGameLevelState();
    resetHallOfFameState();
  }, [resetGameLevelState, resetHallOfFameState]);

  const showTable = list.length > 0;

  return (
    <>
      <Title>Hall of Fame</Title>

      {showTable && (
        <Table>
          <thead>
            <tr>
              <Th align="left">Name</Th>
              <Th align="right">Score</Th>
            </tr>
          </thead>
          <tbody>
            {list.map((winner, index) => (
              <Winner data={winner} key={`${winner.name}-${index}`} />
            ))}
          </tbody>
        </Table>
      )}

      <ButtonContainer>
        <Button onClick={handlePlayAgain}>No limits. Play more!</Button>
      </ButtonContainer>
    </>
  );
};

export default List;
