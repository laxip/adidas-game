export interface HallOfFameUser {
  name: string;
  score: number;
}

export abstract class HallOfFameService {
  public abstract async saveUser(name: string, score: number): Promise<void>;

  public abstract async getList(): Promise<HallOfFameUser[]>;
}
