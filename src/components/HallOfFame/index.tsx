import React, { useCallback, useState, FormEvent, FC, useContext, useRef, useEffect } from 'react';
import { useRecoilValue } from 'recoil';
import ServicesContext, { HALL_OF_FAME_SERVICE } from 'services';
import { gameLevelState } from 'states/gameLevelState';

import { Container, Header, Input, ButtonContainer, Button } from './HallOfFame.atoms';
import { HallOfFameService } from './HallOfFame.types';
import List from './List';

const HallOfFame: FC = () => {
  const service: HallOfFameService = useContext(ServicesContext)[HALL_OF_FAME_SERVICE];

  const [name, setName] = useState<string>('');
  const [showHallOfFame, setShowHallOfFame] = useState<boolean>(false);
  const inputRef = useRef<HTMLInputElement | null>(null);

  const level = useRecoilValue(gameLevelState);

  useEffect(() => {
    inputRef?.current?.focus();
  }, [inputRef]);

  const handleSetName = useCallback(
    (event: FormEvent<HTMLInputElement>) => {
      setName(event.currentTarget.value);
    },
    [setName]
  );

  const showSaveButton = name.length > 0;

  const handleSave = useCallback(
    async (event: FormEvent<HTMLFormElement>) => {
      event.preventDefault();

      if (showSaveButton) {
        await service.saveUser(name, level);

        setShowHallOfFame(true);
      }
    },
    [name, level, showSaveButton, service]
  );

  return (
    <Container>
      {!showHallOfFame && (
        <>
          <Header>
            You have reached to level: <strong>{level}</strong>.
          </Header>

          <form onSubmit={handleSave}>
            Give us your name:
            <Input ref={inputRef} value={name} onChange={handleSetName} />
            {showSaveButton && (
              <ButtonContainer>
                <Button type="submit">Save</Button>
              </ButtonContainer>
            )}
          </form>
        </>
      )}

      {showHallOfFame && <List />}
    </Container>
  );
};

export default HallOfFame;
