import React, { FC } from 'react';
import { useRecoilValue } from 'recoil';
import { gameLevelState } from 'states/gameLevelState';

import { Header } from './GameHeader.atoms';

const GameHeader: FC = () => {
  const level = useRecoilValue(gameLevelState);

  return <Header>Level: {level}</Header>;
};

export default GameHeader;
