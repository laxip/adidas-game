import styled from 'styled-components';

export const Header = styled.div`
  font-family: ${(props) => props.theme.italicFont};
  font-style: normal;
  font-weight: 500;
  font-size: 2rem;
  text-transform: uppercase;
  max-width: ${(props) => props.theme.areaSize}px;
  margin: 1rem auto;
`;
