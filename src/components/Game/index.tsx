import GameArea from 'components/GameArea';
import GameHeader from 'components/GameHeader';
import HallOfFame from 'components/HallOfFame';
import React, { FC } from 'react';
import { useRecoilValue } from 'recoil';
import { showHallOfFameState } from 'states/showHallOfFameState';

const Game: FC = () => {
  const showHallOfFame = useRecoilValue(showHallOfFameState);

  if (showHallOfFame) {
    return <HallOfFame />;
  }

  return (
    <>
      <GameHeader />
      <GameArea />
    </>
  );
};

export default Game;
