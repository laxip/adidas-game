import Game from 'components/Game';
import GlobalStyle from 'components/GlobalStyle';
import ThemeHeader from 'components/ThemeHeader';
import { defaultTheme, darkTheme } from 'config/theme';
import React, { FC } from 'react';
import { useRecoilValue, selector } from 'recoil';
import { themeState, DARK_THEME } from 'states/themeState';
import { ThemeProvider } from 'styled-components';

const themeSelector = selector({
  key: 'themeSelector',
  get: ({ get }) => {
    const mode = get(themeState);

    return mode === DARK_THEME ? darkTheme : defaultTheme;
  },
});

const App: FC = () => {
  const theme = useRecoilValue(themeSelector);

  return (
    <ThemeProvider theme={theme}>
      <GlobalStyle />

      <ThemeHeader />

      <Game />
    </ThemeProvider>
  );
};

export default App;
