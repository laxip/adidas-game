import { hslToColorString } from 'polished';

import { GameConfig } from './GameArea.types';

const FULL_ANGLE = 360;
const SATURATION_MIN = 0.3;
const SATURATION_DELTA = 0.4;
const MIN_NUMBER_OF_TILES_IN_ROW = 2;
const LIGHTNESS_LEVEL = 0.5;
const POWER_BASE = 0.9;
const LEVEL_DELTA = 0.2;

export const prepareGameConfig = (level: number): GameConfig => {
  level = level + MIN_NUMBER_OF_TILES_IN_ROW;

  const hue = Math.random() * FULL_ANGLE;

  // main color components
  const saturation = Math.random() * SATURATION_DELTA + SATURATION_MIN;
  const lightness = LIGHTNESS_LEVEL;

  const length = level * level;

  const scale = Math.pow(POWER_BASE, level);

  // highlighted color components
  const diffSaturation = (Math.random() < 0.5 ? 1 : -1) * scale * LEVEL_DELTA;
  const diffLightness = (Math.random() < 0.5 ? 1 : -1) * scale * LEVEL_DELTA;

  const mainColor = { hue, saturation, lightness };
  const highLightedColor = {
    hue,
    saturation: saturation + diffSaturation,
    lightness: lightness + diffLightness,
  };

  return {
    highlightedColor: hslToColorString(highLightedColor),
    highlightedPosition: Math.floor(Math.random() * length),
    mainColor: hslToColorString(mainColor),
    percentageSize: 100 / level,
    tileCount: length,
  };
};
