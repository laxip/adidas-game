export interface GameConfig {
  highlightedColor: string;
  highlightedPosition: number;
  mainColor: string;
  percentageSize: number;
  tileCount: number;
}
