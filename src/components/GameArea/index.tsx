import React, { FC, useCallback, useMemo } from 'react';
import AutoSizer from 'react-virtualized-auto-sizer';
import { useRecoilState, useSetRecoilState } from 'recoil';
import { gameLevelState } from 'states/gameLevelState';
import { showHallOfFameState } from 'states/showHallOfFameState';

import { Container, Tile } from './GameArea.atoms';
import { prepareGameConfig } from './GameArea.helpers';

const GameArea: FC = () => {
  const [level, setLevel] = useRecoilState(gameLevelState);
  const setShowHallOfFameState = useSetRecoilState(showHallOfFameState);

  const gameConfig = useMemo(() => prepareGameConfig(level), [level]);

  const handleUserClick = useCallback(
    (clicked: number) => {
      if (clicked === gameConfig.highlightedPosition) {
        setLevel(level + 1);
      } else {
        setShowHallOfFameState(true);
      }
    },
    [gameConfig, level, setLevel, setShowHallOfFameState]
  );

  return (
    <AutoSizer
      style={{
        height: '100%',
        width: '100%',
      }}
    >
      {({ height, width }) => (
        <Container sizes={{ width, height }}>
          {[...Array(gameConfig.tileCount)].map((_, index) => (
            <Tile
              key={index}
              position={index}
              config={gameConfig}
              onClick={() => {
                handleUserClick(index);
              }}
            />
          ))}
        </Container>
      )}
    </AutoSizer>
  );
};

export default GameArea;
