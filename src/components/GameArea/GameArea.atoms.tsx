import React from 'react';
import styled from 'styled-components';

import { GameConfig } from './GameArea.types';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export const Container = styled(({ sizes, ...props }) => <div {...props} />)((props) => {
  const size = Math.min(props.theme.areaSize, props.sizes.width);

  return {
    width: `${size}px`,
    height: `${size}px`,
    padding: '5px',
    background: props.theme.bodyBackground,
    overflow: 'hidden',
    margin: 'auto',
    boxSizing: 'border-box',
  };
});

export const Tile = styled.div<{
  config: GameConfig;
  position: number;
  onClick: () => void;
}>`
  width: ${(props) => props.config.percentageSize}%;
  height: ${(props) => props.config.percentageSize}%;
  background: ${(props) =>
    props.position === props.config.highlightedPosition ? props.config.mainColor : props.config.highlightedColor};
  float: left;
  border: 4px solid ${(props) => props.theme.bodyBackground};
  box-sizing: border-box;
  cursor: pointer;
`;
