import styled from 'styled-components';

const BORDER_SIZE = 2;
const PADDING = 4;
const SIZE = 12;

export const Container = styled.div`
  padding: 1rem 0;
  text-transform: uppercase;
  line-height: ${SIZE + 2 * PADDING + 3 /* fix font */}px;

  .switch {
    position: relative;
    display: inline-block;
    width: ${2 * SIZE + 4 * PADDING}px;
    height: ${SIZE + 2 * PADDING}px;
    margin-left: 0.5rem;
  }

  .switch input {
    opacity: 0;
    width: 0;
    height: 0;
  }

  .slider {
    position: absolute;
    cursor: pointer;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-color: ${(props) => props.theme.bodyBackground};
    transition: ${(props) => props.theme.transitionTime}s;
    border-radius: 100px;
    border: 2px solid ${(props) => props.theme.fontColor};
  }

  .slider:before {
    position: absolute;
    content: '';
    height: ${SIZE}px;
    width: ${SIZE}px;
    left: ${PADDING - BORDER_SIZE}px;
    bottom: ${PADDING - BORDER_SIZE}px;
    background-color: ${(props) => props.theme.fontColor};
    transition: ${(props) => props.theme.transitionTime}s;
    border-radius: 50%;
  }

  input:checked + .slider:before {
    transform: translateX(${SIZE + 2 * PADDING}px);
  }
`;
