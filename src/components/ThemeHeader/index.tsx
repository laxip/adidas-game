import React, { FC } from 'react';
import { useSetRecoilState, selector } from 'recoil';
import { themeState, DARK_THEME, DEFAULT_THEME } from 'states/themeState';

import { Container } from './ThemeHeader.atoms';

const changeThemeSelector = selector<void>({
  key: 'changeThemeSelector',
  get: () => {
    /* void */
  },
  set: ({ set, get }) => {
    const theme = get(themeState);

    set(themeState, theme === DARK_THEME ? DEFAULT_THEME : DARK_THEME);
  },
});

const ThemeHeader: FC = () => {
  const setTheme = useSetRecoilState(changeThemeSelector);

  return (
    <Container>
      Switch theme:
      <label className="switch">
        <input
          type="checkbox"
          onChange={() => {
            setTheme();
          }}
        />
        <span className="slider" />
      </label>
    </Container>
  );
};

export default ThemeHeader;
