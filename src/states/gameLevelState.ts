import { atom } from 'recoil';

export const gameLevelState = atom<number>({
  key: 'gameLevelState',
  default: 0,
});
