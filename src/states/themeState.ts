import { atom } from 'recoil';

export const DEFAULT_THEME = 'DEFAULT_THEME';
export const DARK_THEME = 'DARK_MODE';

export const themeState = atom({
  key: 'themeState',
  default: DEFAULT_THEME,
});
