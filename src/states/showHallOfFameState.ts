import { atom } from 'recoil';

export const showHallOfFameState = atom<boolean>({
  key: 'showHallOfFameState',
  default: false,
});
