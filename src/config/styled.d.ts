import 'styled-components';

declare module 'styled-components' {
  export interface DefaultTheme {
    areaSize: number;
    bodyBackground: string;
    fontColor: string;
    regularFont: string;
    specialFont: string;
    italicFont: string;
    transitionTime: number;
  }
}
