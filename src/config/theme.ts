import { DefaultTheme } from 'styled-components';

export const defaultTheme: DefaultTheme = {
  areaSize: 400,
  bodyBackground: '#ffffff',
  fontColor: '#000000',
  italicFont: 'AdihausDIN Cn, Helvetica, Arial, sans-serif',
  regularFont: 'AdineuePRO, Helvetica, Arial, sans-serif',
  specialFont: 'AdihausDIN, Helvetica, Arial, sans-serif',
  transitionTime: 0.4,
};

export const darkTheme: DefaultTheme = {
  ...defaultTheme,
  bodyBackground: '#000000',
  fontColor: '#ffffff',
};
