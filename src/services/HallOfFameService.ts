interface HallOfFameServiceUser {
  name: string;
  score: number;
}

class HallOfFameService {
  public static storageKey = 'HALL_OF_FAME';

  public static howManyElements = 10;

  static getDataFromStorage(): HallOfFameServiceUser[] {
    const storage = localStorage.getItem(HallOfFameService.storageKey);
    let items: HallOfFameServiceUser[] = [];

    try {
      items = JSON.parse(storage || '[]');
    } catch (e) {
      console.error(e);
    }

    return items;
  }

  public static async saveUser(name: string, score: number): Promise<void> {
    const items = HallOfFameService.getDataFromStorage();

    items.push({
      name,
      score,
    });

    localStorage.setItem(HallOfFameService.storageKey, JSON.stringify(items));
  }

  public static async getList(): Promise<HallOfFameServiceUser[]> {
    const items = HallOfFameService.getDataFromStorage();

    return items.sort(HallOfFameService.sortFunction).slice(0, HallOfFameService.howManyElements);
  }

  public static sortFunction(a: HallOfFameServiceUser, b: HallOfFameServiceUser): number {
    return b.score - a.score;
  }
}

export default HallOfFameService;
