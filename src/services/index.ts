import { createContext } from 'react';

import HallOfFameService from './HallOfFameService';

export const HALL_OF_FAME_SERVICE = 'HALL_OF_FAME_SERVICE';

export const services = {
  HALL_OF_FAME_SERVICE: HallOfFameService,
};

export default createContext(services);
