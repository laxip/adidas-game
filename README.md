# Adidas Game

## Demo

https://laxip.gitlab.io/adidas-game/

## How to run?

This is regular `create-react-app` project. To run use commands:

```
yarn
yarn start
```

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
To see more please visit [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

## Approach 

I used `recoil` for state management. Instead of using *PostCSS* I decided to use `styled-components` and I wrote
functionality of changing themes from dark to light. Service which allows us to store users in
hall of fame is injected via context. Also I used helpers for colors from `polished`. Code is wrapped by
`Error Boundary` to handle any error from *react*. Size of game area is calculated via `virtualized autosizer`.

I dind't write extra `the script to win the game` because nobody is able to win this game. 
In requirements, there isn't stop condition, so we can change size of tile whole time. 

I believe that the simplest solutions are the best. In this app, files are short, and each of them 
is separated. 

## Quality

There is `eslint` extended with `prettier` and `sorting imports`. Also I added `husky` and `lint-staged` to 
disallow developers to commit code without checking quality.

## Potential risk
I used constant size of border for each tile. If tile is very small, border could be bigger and
user will be not able to see tile. In production app I would use dynamic size of border, but there is still 
one issue. If screen's width is eg 400px, we cannot show more then 400 tiles. 

## Design

I had inspiration from: https://www.adidas.pl/
